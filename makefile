# make for my1load86

MY1MBRBIN = mbrbin.bin
MY1LOADER = loader.bin
MY1LDNAME = ldplayos
MY1LOAD2R = $(MY1LDNAME).sys
MY1USBDRV = disk.img
SRC4LOAD2 = src/display.asm src/disk.asm

DD_OPT_MBR1 = bs=1 count=446 conv=notrunc
DD_OPT_MBR2 = bs=1 count=2 conv=notrunc skip=510 seek=510
# first partition is at sector 2048 = 2048 x 512 = 1048576!
DD_OPT_LDR1 = bs=1 count=3 conv=notrunc seek=1048576
DD_OPT_LDR2 = bs=1 count=422 conv=notrunc skip=90 seek=1048666
# to create loop device, losetup -o 1048576 /dev/loop0 $(MY1USBDRV)
# to list loop devices, losetup -a
# mount as usual, do what is needed...
# to delete loop device, losetup -d /dev/loop0
AS = nasm
ASFLAGS = -i src/

# stuff for dummy my1playos kernel

MY1SOURCE = my1os
MY1OBJECT = entry.o main.o display.o
MY1KERNEL = $(MY1SOURCE).elf

CFLAGS = -Wall -Wextra -m32
CFLAGS += -ffreestanding -nostdinc -fno-stack-protector
# -fno-builtin => implied by -ffreestanding
# -fno-hosted => equivalent to -ffreestanding
LDFLAGS = -Tsrc/linker.ld
# -s | --strip-all # strip all symbol
# -S | --strip-debug # strip debug symbols
LDFLAGS += -s -m elf_i386
AOFLAGS = -felf
# LFLAGS meant for linking using gcc
LFLAGS = -nostartfiles -nodefaultlibs -nostdlib
CC = gcc
LD = ld

.PHONY: mbrbin loader load2r kernel

all: mbrbin loader load2r kernel

set: mbr load load2r kernel

image: $(MY1USBDRV)

mbrbin: $(MY1MBRBIN)

loader: $(MY1LOADER)

load2r: $(MY1LOAD2R)

kernel: $(MY1KERNEL)

mbr: mbrbin image
	@echo -n "Copying '$(MY1MBRBIN)' to '$(MY1USBDRV)'... "
	@dd if=$(MY1MBRBIN) of=$(MY1USBDRV) $(DD_OPT_MBR1) 2>/dev/null
	@dd if=$(MY1MBRBIN) of=$(MY1USBDRV) $(DD_OPT_MBR2) 2>/dev/null
	@echo "done!"

load: loader image
	@echo -n "Copying '$(MY1LOADER)' to '$(MY1USBDRV)'... "
	@dd if=$(MY1LOADER) of=$(MY1USBDRV) $(DD_OPT_LDR1) 2>/dev/null
	@dd if=$(MY1LOADER) of=$(MY1USBDRV) $(DD_OPT_LDR2) 2>/dev/null
	@echo "done!"

play: load2r kernel image
#@sh util/init_disk_image.sh $(MY1USBDRV)
	@echo -n "Copying loader & kernel to '$(MY1USBDRV)'... "
	@mcopy -i $(MY1USBDRV)@@1048576 -o -n $(MY1LOAD2R) $(MY1KERNEL) ::
	@echo "done!"
#mdir -i $(MY1USBDRV)@@1048576
#mdel -i $(MY1USBDRV)@@1048576 ::$(MY1LOAD2R) ::$(MY1KERNEL)

new: clean all

clean:
	rm -rf *.o *.bin *.sys *.lst *.elf

sweep: clean
	rm -rf *.img

dump: $(MY1USBDRV)
	#hexdump -C $(MY1USBDRV)
	hexdump -C $(MY1USBDRV) -n 512 -s 0
	hexdump -C $(MY1USBDRV) -n 512 -s 1048576

test: mbr load play
	@sh util/test_disk_image.sh $(MY1USBDRV)

$(MY1LOAD2R): src/$(MY1LDNAME).asm $(SRC4LOAD2)
	$(AS) $(ASFLAGS) -f bin $< -o $@

%.bin: src/%.asm
	$(AS) $(ASFLAGS) -f bin $< -o $@

%.sys: src/%.asm
	$(AS) $(ASFLAGS) -f bin $< -o $@

%.lst: src/%.asm
	$(AS) $(ASFLAGS) -f bin $< -l $@

$(MY1KERNEL): $(MY1OBJECT)
	$(LD) -o $@ $(LDFLAGS) $+

%.o: src/%.c
	$(CC) $(CFLAGS) -c $<

%.o: src/%.asm
	$(AS) $(AOFLAGS) $< -o $@

$(MY1USBDRV):
	@sh util/make_disk_image.sh --fat32 --image $@
