ATA_IOPRI equ 0x1f0
ATA_IOSEC equ 0x170
ATA_RDATA equ 0x0
ATA_RFEAT equ 0x1
ATA_RERRO equ 0x1
ATA_RSCNT equ 0x2
ATA_RLBAL equ 0x3
ATA_RLBAM equ 0x4
ATA_RLBAH equ 0x5
ATA_RDRVH equ 0x6
ATA_RSTAT equ 0x7
ATA_RCOMD equ 0x7

ATA_IOSEL equ ATA_IOPRI
ATA_BUSY  equ 0x80
ATA_READY equ 0x40
ATA_CMD_READ equ 0x20

; should detect this???
SECT_SIZE equ 0x200

waitbusy:
	push edx
	mov dx,(ATA_IOSEL+ATA_RSTAT)
.loop:
	in al,dx
	and al,(ATA_BUSY|ATA_READY)
	cmp al,ATA_READY
	jnz .loop
	pop edx
	ret

; destination is es:edi
readsect:
	push ecx
	push edx ; should contain offset
	call waitbusy
	mov dx,(ATA_IOSEL+ATA_RSCNT)
	mov al,1
	out dx,al
	inc dx
	mov al,[ esp ]
	out dx,al
	inc dx
	mov al,[ esp + 1 ]
	out dx,al
	inc dx
	mov al,[ esp + 2 ]
	out dx,al
	inc dx
	mov al,[ esp + 3 ]
	or al,0xe0
	out dx,al
	inc dx
	mov al,ATA_CMD_READ
	out dx,al
	call waitbusy
	; read it!
	mov edx,(ATA_IOSEL+ATA_RDATA)
	mov ecx,SECT_SIZE/4
	repnz insd
	pop edx
	pop ecx
	ret

; structure for FAT common - definition only!
struc fat_common ; BIOS parameter block
	.jmpnop: resb 3 ; jmp nop
	.oem_id: resb 8 ; oem id string
	.bpsect: resw 1 ; bytes per sector : 512,1024,2048,4096
	.spclus: resb 1 ; sector per cluster : 1,2,4,8,16,32,64,128
	.rsccnt: resw 1 ; # of reserved sectors (FAT12/16=>1, FAT32=>32)
	.fatcnt: resb 1 ; # of fat (always 2?)
	.rooten: resw 1 ; root entries (fat16=>512,fat32=>0)
	.sctsma: resw 1 ; sectors (fat32=>0, also 0 if smaller than 32M?)
	.medtyp: resb 1 ; media type (f0-floppy, f8-hard disk, fa-ramdisk?)
	.scpfat: resw 1 ; sectors per fat (on small vols, fat32=>0)
	.scptrk: resw 1 ; sectors per track (63?)
	.heads_: resw 1 ; heads
	.hidsec: resw 1 ; hidden sectors
endstruc ; size = 30 bytes

; structure for FAT32 - definition only!
struc fat_fat32
	.biospb: resb fat_common_size
	.hidsec: resw 1  ; hidden sectors (extension?)
	.sctlrg: resd 1  ; sectors (large volumes)
	.scpfat: resd 1  ; sectors per fat
	.m_flag: resw 1  ; mirror flags (b15-b8:rsvd) (b6-b4:rsvd)
					 ; (b7:1=>single active fat,0=>all fats updated @runtime)
					 ; (b3-b0: # of active fat if b7=1)
	.fsvers: resw 1  ; fs version
	.clroot: resd 1  ; first cluster of root directory (usu. 2)
	.fsinfo: resw 1  ; fs info sector num in fat rsvd area (usu. 1)
	.backbs: resw 1  ; backup boot sector (0xFFFF if none, usu. 6)
	.rsvd_1: resb 12 ; reserved
	.drvnum: resb 1  ; logical drive num - for use with int13
	.rsvd_2: resb 1  ; reserved
	.extsig: resb 1  ; (0x29) ext sig indicate presence (next 3 fields)
	.sernum: resd 1  ; serial number for partition
	.vollbl: resb 11 ; volume label or "NO NAME    "
	.fstype: resb 8  ; fs type "FAT32   "
endstruc ; size = 90 bytes

; structure for fat32 dir entry
struc fat32dir
	.name: resb 11 ; 8+3, whitespace padded for 8
	.attr: resb 1 ; 6-bits actually?
	.non1: resb 10 ; reserved?
	.time: resw 1 ; 5/6/5 => hh/mm/ds
	.date: resw 1 ; 7/4/5 => yy-1980/mm/dd
	.clus: resw 1 ; first cluster
	.size: resd 1 ; size
endstruc

; structure for ELF header - definition only!
struc elf32_header
	.elf_magic: resb 4 ; ELF magic: 0x7F,'E','L','F'
	.elf_class: resb 1 ; class: 1=>32-bit,2=>64-bit
	.elf_edata: resb 1 ; data: 1=>little endian,2=>big endian
	.elf_versi: resb 1 ; version: 1=>original elf
	.elf_ckabi: resb 1 ; ABI type: 0=>sysv,3=>linux,9=>freebsd, usu. 0!
	.elf_exabi: resb 1 ; ABI extension: usu. unused => 0!
	.elf_xxpad: resb 7 ; unused: zero pads
; multi-byte accesses depend on edata entry
	.elf_etype: resw 1 ; type: 1=>relocatable,2=>executable,3=>shared,4=>core
	.elf_ckisa: resw 1 ; instruction set arch: 0x03(x86),0x28(arm),0x3e(x86_64)
	.elf_verso: resd 1 ; version: 1=>original elf
; these are different on elf64 (addresses!)
	.elf_entry: resd 1 ; entry: start of execution
	.elf_phoff: resd 1 ; start of program header
	.elf_shoff: resd 1 ; start of section header
; back to common ground
	.elf_flags: resd 1 ; arch dependent
	.elf_hsize: resw 1 ; size of this header: 52(elf32) 64(elf64)
	.elf_phsiz: resw 1 ; size of program header table
	.elf_phnum: resw 1 ; number of entry in program header table
	.elf_shsiz: resw 1 ; size of section header table
	.elf_shnum: resw 1 ; number of entry in section header table
	.elf_snidx: resw 1 ; index for section header entry with names!
endstruc ; size = 52 bytes

; structure for ELF program header - definition only!
struc elf32_seghead
	.p_type: resd 1 ; segment type
	.p_offs: resd 1 ; file offset
	.p_vadd: resd 1 ; virtual address
	.p_padd: resd 1 ; physical address
	.p_fsiz: resd 1 ; size in file
	.p_msiz: resd 1 ; size in memory
	.p_flag: resd 1 ; flags
	.p_algn: resd 1 ; align
endstruc
