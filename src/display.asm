; NOTE:
; - regarding in and out instructions
;   = valid pc i/o ports are 0-1024 (the rest up to 65535 can be customized?)

VGA_MEM equ 0xb8000
VGA_CTRL equ 0x3d4
VGA_DATA equ 0x3d5
VGA_HIDX equ 14
VGA_LIDX equ 15
VGA_CONW equ 80
VGA_CONH equ 25
TAB_SPACE equ 4
COLOR_WHITE equ 0xF
COLOR_BLACK equ 0x0
ASCII_SPACE equ 0x20
ASCII_BACKSPACE equ 0x08
ASCII_TAB equ 0x09
ASCII_CR equ 0x0d
ASCII_LF equ 0x0a

%ifdef LDPLAYOS
VGA_XPOS db 0
VGA_YPOS db 0
VGA_ATTR dw 0
VGA_BLNK dw 0
VGA_TROW dw 0
VGA_TCOL dw 0
%else
extern VGA_XPOS
extern VGA_YPOS
extern VGA_ATTR
extern VGA_BLNK
extern VGA_TROW
extern VGA_TCOL
%endif

; routine to initialize display
global display_init
display_init:
	push cx
	push dx
	; output byte in al to port [dx] - set to get pos (high byte)
	mov dx,VGA_CTRL
	mov al,VGA_HIDX
	out dx,al
	; input byte from port [dx] to al - get pos (high byte)
	xor ax,ax
	mov dx,VGA_DATA
	in al,dx
	shl ax,8
	mov [ VGA_TROW ],ax
	; output byte in al to port [dx] - set to get pos (low byte)
	mov dx,VGA_CTRL
	mov al,VGA_LIDX
	out dx,al
	; input byte from port [dx] to al - get pos (low byte)
	xor ax,ax
	mov dx,VGA_DATA
	in al,dx
	or ax,[ VGA_TROW ]
	; divide width to get row (quotient), col (remainder)
	xor dx,dx
	mov cx,VGA_CONW
	div cx
	; quotient in ax, remainder in dx
	mov [ VGA_TROW ],ax
	mov [ VGA_TCOL ],dx
	; set attribute here?
	xor ax,ax
	mov ah,(COLOR_BLACK<<4)|COLOR_WHITE
	mov [ VGA_ATTR ],ax
	; prepare blank character
	or ax,ASCII_SPACE
	mov [ VGA_BLNK ],ax
	; done
	pop dx
	pop cx
	ret

; routine to move cursor
move_cursor:
	push dx
	push cx
	; get current position - set in cx
	mov ax,[ VGA_YPOS ]
	mov cx,VGA_CONW
	mul cx
	add ax,[ VGA_XPOS ]
	mov cx,ax
	; set to set pos (high byte)
	mov dx,VGA_CTRL
	mov al,VGA_HIDX
	out dx,al
	; set pos (high byte)
	mov dx,VGA_DATA
	mov ax,cx
	shr ax,8
	out dx,al
	; set to set pos (low byte)
	mov dx,VGA_CTRL
	mov al,VGA_LIDX
	out dx,al
	; set pos (low byte)
	mov dx,VGA_DATA
	mov ax,cx
	out dx,al
	; done
	pop cx
	pop dx
	ret

; routine to scroll
scroll:
	push edx
	push esi
	; check if we need to scroll
	mov dl,[ VGA_YPOS ]
	cmp dl,VGA_CONH
	jc .skip
	; prepare to copy lines
	mov cx,(VGA_CONH-1)*VGA_CONW
	mov esi,VGA_MEM
.loop:
	mov ax,[ esi+VGA_CONW*2 ]
	mov [ esi ],ax
	inc esi
	inc esi
	dec cx
	jnz .loop
	; 'draw' last blank line
	mov cx,VGA_CONW
	mov dx,[ VGA_BLNK ]
.last:
	mov [ esi ],dx
	inc esi
	inc esi
	dec cx
	jnz .last
	; set y position to last line
	mov dl,VGA_CONH-1
	mov [ VGA_YPOS ],dl
.skip:
	pop esi
	pop edx
	ret

; routine to clear display
global display_clear
display_clear:
	push dx
	push cx
	push esi
	; loop through while writing blanks
	mov dx,[ VGA_BLNK ]
	mov cx,VGA_CONH*VGA_CONW
	mov esi,VGA_MEM
.loop:
	mov [ esi ],dx
	inc esi
	inc esi
	dec cx
	jnz .loop
	; reset cursor position
	xor ax,ax
	mov [ VGA_XPOS ],al
	mov [ VGA_YPOS ],al
	; move to that position
	call move_cursor
	pop esi
	pop cx
	pop dx
	ret

; routine to get current pixel memory position
get_pixmem:
	push edx
	xor eax,eax
	mov al,[ VGA_YPOS ]
	xor edx,edx
	mov dl,VGA_CONW
	mul dl
	xor edx,edx
	mov dl,[ VGA_XPOS ]
	add edx,eax
	shl edx,1 ; 2-bytes per position
	mov eax,VGA_MEM
	add eax,edx
	pop edx
	ret

; routine to send a char (in al) to display
global display_put
display_put:
	push dx
	push cx
	push esi
	xor cx,cx
	mov cl,al ; use cl for cmp!
.chk_cr:
	cmp cl,ASCII_CR
	jnz .chk_lf
	xor al,al
	mov [ VGA_XPOS ],al
	jmp .skip
.chk_lf:
	cmp cl,ASCII_LF
	jnz .chk_tab
	xor al,al
	mov [ VGA_XPOS ],al
	inc byte[ VGA_YPOS ]
	jmp .skip
.chk_tab:
	cmp cl,ASCII_TAB
	jnz .chk_bsp
	mov al,[ VGA_XPOS ]
	mov dl,TAB_SPACE
	add al,dl
	dec dl
	not dl
	and al,dl
	mov [ VGA_XPOS ],al
	jmp .skip
.chk_bsp:
	cmp cl,ASCII_BACKSPACE
	jnz .chk_txt
	mov al,[ VGA_XPOS ]
	or al,al
	jz .skip
	dec al
	mov [ VGA_XPOS ],al
	; should blank this location?
	jmp .skip
.chk_txt:
	cmp cl,' '
	jc .no_char
	cmp cl,0x80 ; valid ascii char (<128) only?
	jc .put_chr
.no_char:
	mov cl,'?'
.put_chr:
	call get_pixmem
	mov esi,eax
	or cx,[ VGA_ATTR ]
	mov [ esi ],cx
	inc byte[ VGA_XPOS ]
.skip:
	mov al,[ VGA_XPOS ]
	cmp al,VGA_CONW
	jc .done
	xor al,al
	mov [ VGA_XPOS ],al
	inc byte[ VGA_YPOS ]
.done:
	call scroll
	call move_cursor
	pop esi
	pop cx
	pop dx
	ret

; routine to send a string to display
global display_write
display_write:
	push esi
	mov esi,[ esp+8 ]
.loop:
	lodsb
	or al, al
	jz short .done
	call display_put
	jmp short .loop
.done:
	pop ebx
	ret

; routine to display hex value of lower nibble in al
display_value_nibb:
	and al,0x0f
	add al,0x30
	cmp al,0x3a
	jc short .this
	add al,0x27
.this:
	call display_put
	ret

; routine to display hex value of byte in al
global display_value_byte
display_value_byte:
	push bx
	mov bl,al
	; show upper nibble
	shr al,4
	call display_value_nibb
	; show lower nibble
	mov al,bl
	call display_value_nibb
	pop bx
	ret

; routine to display hex value of word in ax
global display_value_word
display_value_word:
	push bx
	mov bx,ax
	mov al,bh
	call display_value_byte
	mov al,bl
	call display_value_byte
	mov ax,bx
	pop bx
	ret

; routine to display hex value of eax
global display_value_hexa
display_value_hexa:
	push ebx
	mov ebx,eax
	shr eax,16
	call display_value_word
	mov ax,bx
	call display_value_word
	mov eax,ebx
	pop ebx
	ret

; routine to send newline character to display
global display_newline
display_newline:
	;mov al,0x0d
	;call display_put
	mov al,0x0a
	call display_put
	ret
