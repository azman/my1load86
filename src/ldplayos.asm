bits 16
org 0x8000

; objective: able to boot my1playos
;   - fs support: fat32 and ext2/3/4?
;   - load it to 0x100000 (therefore needs protected mode!)
;   - comply with multiboot?
;   - but, for now... just show it got loaded!

; first stage loader should still be there at 0x7c00!
;   - this stage is at 0x0000:0x8000

INITFULL equ 0x8000
STACKBEG equ INITFULL
; this code should be <((INITTEMP-INITFULL)=4KB)
INITTEMP equ 0x9000
ELFSTEMP equ 0xA000 ; staging for offset reading
ELFSTAGE equ 0x10000 ; staging for loading kernel

BOOTPREV equ 0x7c00
STACKOLD equ BOOTPREV
BOOT_LBA equ (STACKOLD-6)

init:
; make sure everybody on the same page :p
	mov ax,cs
	mov ds,ax
	mov es,ax
	mov ss,ax
	mov sp,STACKBEG
	jmp a20f

; sub routine to check if a20 address line has been enabled
check_a20:
	push ds
	push es
	xor ax,ax
	mov es,ax
	not ax
	mov ds,ax
	mov di,0x0500
	mov si,0x0510
	mov al,byte[es:di]
	mov ah,byte[ds:si]
	push ax
	mov byte[es:di], 0x00
	mov byte[ds:si], 0xFF
	cmp byte[es:di], 0xFF
	pop ax
	mov byte[es:di],al
	mov byte[ds:si],ah
	xor ax,ax ; by default a20 assumed disabled
	je .skip
	inc ax ; a20 is enabled!
.skip:
	pop es
	pop ds
	ret

; definitions for gdt entry
GDT_ACCESS_PRESENT            equ 0x80
GDT_ACCESS_SEG_TYPE_MASK      equ 0x0F
GDT_ACCESS_PRIVILEGE_MASK     equ 0x60
GDT_ACCESS_PRIVILEGE_RL0      equ 0x00
GDT_ACCESS_PRIVILEGE_RL1      equ 0x20
GDT_ACCESS_PRIVILEGE_RL2      equ 0x40
GDT_ACCESS_PRIVILEGE_RL3      equ 0x60
GDT_ACCESS_SEG_NOT_SYSTEM     equ 0x10 ; 1 = code/data segment!
GDT_ACCESS_SEG_TYPE_CODE      equ 0x08
GDT_ACCESS_SEG_CODE_CONFORM   equ 0x04
GDT_ACCESS_SEG_CODE_READ      equ 0x02
GDT_ACCESS_SEG_TYPE_EXEC      equ 0x08
GDT_ACCESS_SEG_DATA_XPND_DOWN equ 0x04
GDT_ACCESS_SEG_DATA_WRITE     equ 0x02
GDT_ACCESS_SEG_TYPE_ACCESS    equ 0x01
GDT_GRANULARITY_SEG_LEN_MASK  equ 0x0F
GDT_GRANULARITY_GRAN_4KB      equ 0x80
GDT_GRANULARITY_SEGS_32B      equ 0x40
GDT_GRANULARITY_RSVD_64B      equ 0x20
GDT_GRANULARITY_SYST_AVL      equ 0x10

; simple gdt for initial protected mode entry
; - this must be here (in 16-bit code area?)
; - triple faults if moved to further up (32-bit area?)
gdt_beg:
; null descriptor
	dd 0,0
; kernel code { limit_l=0xFFFF, base_l=0x0000, base_m=0x00,
;               access=0x9A (GDT_ACCESS_PRESENT,
;                            GDT_ACCESS_PRIVILEGE_RL0,
;                            GDT_ACCESS_SEG_NOT_SYSTEM,
;                            GDT_ACCESS_SEG_TYPE_CODE,
;                            GDT_ACCESS_SEG_CODE_READ),
;               granularity=0xCF (GDT_GRANULARITY_GRAN_4KB,
;                                 GDT_GRANULARITY_SEGS_32B,
;                                 SEG_LEN/limit_h=0xF),
;               base_h=0x00 } => base=0x00000000 limit=0xFFFFF
	db 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x9A, 0xCF, 0x00
; kernel data { limit_l=0xFFFF, base_l=0x0000, base_m=0x00,
;               access=0x92 (GDT_ACCESS_PRESENT,
;                            GDT_ACCESS_PRIVILEGE_RL0,
;                            GDT_ACCESS_SEG_NOT_SYSTEM,
;                            GDT_ACCESS_SEG_DATA_WRITE),
;               granularity=0xCF (GDT_GRANULARITY_GRAN_4KB,
;                                 GDT_GRANULARITY_SEGS_32B,
;                                 SEG_LEN/limit_h=0xF),
;               base_h=0x00 } => base=0x00000000 limit=0xFFFFF
	db 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x92, 0xCF, 0x00
gdt_end:

; 'pointer' to simple gdt
gdt_ptr:
	dw gdt_end - gdt_beg - 1
	dd gdt_beg

; enable a20 line using fast a20 method
a20f:
	in al,0x93
	test al,2
	jnz prep
	or al,2
	and al,0xFE
	out 0x92,al
wait_a20:
	call check_a20
	jz wait_a20

prep:
	cli
	lgdt [gdt_ptr]
; going protected mode
	mov eax, cr0
	or eax, 1
	mov cr0, eax
; using descriptor instead of pure segment
	jmp 0x08:pmode

bits 32

pmode:
	mov eax, 0x10
	mov ds, eax
	mov es, eax
	mov ss, eax
	mov fs, eax
	mov gs, eax
	mov esp,STACKBEG
	jmp next

%define LDPLAYOS
%include "display.asm"

; system message
msg db 'MY1 Loader v1.0 by azman@my1matrix.org',10,0

next:
	call display_init
	call display_clear
	mov esi,msg
	call display_text
	jmp read

display_text:
	push esi
	call display_write
	pop esi
	ret

%include "disk.asm"

; temp storage
tmp dd 0

read:
; read sector!
	mov edi,INITTEMP
	;xor edx,edx
	mov edx,[ BOOT_LBA ]
	call readsect
; show sector!
;	mov dword[ tmp ],INITTEMP
;	mov ecx,512
;.loop:
;	; check newline every 16 bytes
;	mov ax,cx
;	mov bl,16
;	div bl ; remainder in ah
;	or ah,ah
;	jnz .skip
;	call display_newline
;	mov eax,[ tmp ]
;	call display_value_hexa
;	mov al,' '
;	call display_put
;.skip:
;	mov edi,[ tmp ]
;	mov al,[ edi ]
;	call display_value_byte
;	inc dword[ tmp ]
;	mov al,' '
;	call display_put
;	dec ecx
;	jnz .loop
	jmp look

; system messages
yay db 'Yay!',10,0
nay db 10,'Nay!',10,0
got db 10,'Loading ',0
dot db '... ',0
e1r db 10,'Unknown FS!',10,0
e2r db 10,'Cannot find kernel!',10,0
e3r db 10,'File read error!',10,0
e4r db 10,'Invalid ELF header!',10,0
e5r db 10,'Page read error!',10,0
e6r db 10,'System Halted!',10,0

; check fs type
tgt db 'FAT32   ',0

; check if vbr is from fat32 (future will consider linux ext fs)
look:
	mov esi,INITTEMP+fat_fat32.fstype
	mov edi,tgt
	mov ecx,8
	repe cmpsb
	jcxz fat32
; checkit!
;	mov esi,INITTEMP+fat_fat32.fstype
;	mov edi,tgt
;	mov ecx,8
;	rep movsb  ; ds:si to es:di, both si & di auto-inc!
;	mov esi,tgt
;	call puts
;	mov al,' '
;	call display_put
	mov esi,e1r
	jmp say_halt

; useful temp storage
FAT_INIT: dd 0 ; fat32 fat lba address (sector index)
DAT_INIT: dd 0 ; fat32 data area lba address
CHK_CLUS: dd 0 ; currect cluster number within DATA
CHK_SECT: dd 0 ; current sector being accessed (within CHK_CLUS)
CHK_FD32: dd 0
CHK_SIZE: dd 0
END_BUFF: dd 0
FAT_SECT: dd 0 ; current sector access within FAT
; target loader file to look for
CHK_FILE: db 'MY1OS   ELF',0

INITBUFF equ (INITTEMP+SECT_SIZE)

; prepare to browse fat32
fat32:
	mov eax,[ BOOT_LBA ]
; get fat starting lba
	xor ecx,ecx
	mov cx,[ INITTEMP + fat_common.rsccnt ]
	add eax,ecx
	mov [ FAT_INIT ],eax
; get fat count
	xor eax,eax
	mov al,[ INITTEMP + fat_common.fatcnt ]
; get fat size (in sector count)
	mov cx,[ INITTEMP + fat_fat32.scpfat ]
	mul ecx ; # of fat x sector per fat
	add eax,[ FAT_INIT ]
	mov [ DAT_INIT ],eax
; get sector size - calculate INITBUFF's end point
	xor eax,eax
	mov ax,[ INITTEMP + fat_common.bpsect ] ; sector size in bytes
	add eax,INITBUFF
	mov [ END_BUFF ],eax
; get first cluster for root directory
	mov eax,[ INITTEMP + fat_fat32.clroot ]
	mov [ CHK_CLUS ],eax
.next: ; start reading cluster
	; get cluster size
	mov cl,[ INITTEMP + fat_common.spclus ]
	mov [ CHK_SIZE ],cl
	call cl2sect
.read: ; read single sector
	mov edi,INITBUFF
	mov edx,[ CHK_SECT ]
	call readsect
	; browse root dir info (1 sector)
	mov dword[ CHK_FD32 ],INITBUFF ; sector is in buff
	call find32file
	jz found
	jnc .done
	; done with a sector, check end of cluster
	dec byte[ CHK_SIZE ]
	jz short .over ; to next cluster?
	inc dword[ CHK_SECT ]
	jmp short .read
.over: ; get next cluster!
	call cl2next
	jnz .next
.done:
	mov esi,e2r
	jmp say_halt

; subroutine to get cluster's sector address (lba)
cl2sect:
	mov eax,[ CHK_CLUS ]
	sub eax,2 ; cluster 0 & 1 is unused - cluster 2 at beginning of data
	xor edx,edx
	mov dl,[ INITTEMP + fat_common.spclus ]
	mul edx
	add eax,dword[ DAT_INIT ]
	mov [ CHK_SECT ],eax
	ret

; subroutine to get next cluster (Z=0 means success)
cl2next:
	mov eax,[ CHK_CLUS ]
	shl eax,2 ; times 4 - get fat offset 32-bit per cluster (28 actually)
	mov [ tmp ],eax
	; get sector number containing the cluster number
	xor edx,edx
	xor ecx,ecx
	mov cx,[ INITTEMP + fat_common.bpsect ] ; sector size in bytes
	div cx  ; sector offset in ax, byte offset within sector in dx
	;mov cx,[ INITTEMP + fat_fat32.scpfat ]
	add eax,[ FAT_INIT ]
	mov ebx,edx
	mov edx,eax
	mov edi,INITBUFF
	call readsect
	mov esi,ebx
	mov eax,[ esi+INITBUFF ] ; get cluster number
	cmp eax,0xFFFFFFFF ; actually >0xFFFFFFF8 ?
	jz short .done
	; get next cluster!
	and eax,0x0FFFFFFF ; 28-bits?
	mov [ CHK_CLUS ],eax
	or eax,eax ; can never be zero coz cluster is never 0 (or 1)!
.done:
	ret

; sub-routine to find file in 512/32 fat32 dir entry
find32file:
	mov esi,[ CHK_FD32 ]
	mov al,byte[ esi ]
	cmp al,0xe5 ; check if not used
	jz short .skip
	cmp al,0x00 ; done if at end of directory
	jz short .done
	; check if subdir? - check attrib?
	mov al,byte[ esi + 11 ]
	and al,0x08
	jnz .skip
	; check if this is target file
	mov edi,CHK_FILE ; esi already set!
	mov ecx,11
	repe
	cmpsb
	jcxz .yess
	; move for printing FOR NOW...
	;mov edi,CHK_FILE
	;mov ecx,11
	;rep movsb  ; ds:si to es:di, both si & di auto-inc!
	;mov esi,CHK_FILE
	;call display_text
	;call display_newline
.skip:
	mov eax,[ END_BUFF ]
	add dword[ CHK_FD32 ],32 ; this is fat32 dir entry size
	cmp dword[ CHK_FD32 ],eax
	jc short find32file ; false if dx==buff+512!
	; end of sector?
	xor eax,eax
	inc eax
	stc ; set carry flag
	jmp short .back
.done: ; end of directory?
	xor eax,eax
	inc eax
	; make sure not z and no c?
	jmp short .back
.yess: ; found it!
	xor eax,eax
.back:
	ret

show_fat32name:
	push ebx
	push ecx
	mov ebx,esi
	mov ecx,8
.loop:
	mov al,[ esi ]
	or al, al
	jz short .done
	cmp al,' '
	jz .skip
	call display_put
.skip:
	dec ecx
	jnz .next
	mov al,'.'
	call display_put
	mov ecx,3
.more:
	inc ebx
	mov esi,ebx
	mov al,[ esi ]
	or al, al
	jz short .done
	call display_put
	dec ecx
	jnz .more
	jmp short .done
.next:
	inc ebx
	mov esi,ebx
	jmp short .loop
.done:
	pop ecx
	pop ebx
	ret

ELFMAGIC:
	dd 0x464C457F ; 0x7F,'E','L','F'
ELFCHECK:
	dd 0x00010101 ; ABI:sysv,vers:original,endian:little,class:32-bit
; variable storage
ELF_SIZE: dd 0 ; may not need this
ELF_INIT: dd 0 ; program entry
ELF_CODE: dd 0 ; program header count
ELF_OFFS: dd 0 ; program header offset
ELF_TEMP: dd 0 ; temp dst addr used by load4kpage
CHK_TEMP: dd 0
ELF_HSIZ: dd 0
SEG_HEAD: dd 0
TGT_INIT: dd 0 ; load destination add
TGT_SIZE: dd 0 ; load destination size
PHY_INIT: dd 0 ; program entry @ physical location
CUR_SIZE: dd 0 ; keep track read size within 4k-page loading
FIX_CLUS: dd 0 ; keep beginning of file cluster

found:
; say we got this...
	mov esi,got
	call display_text
	mov esi,[ CHK_FD32 ]
	call show_fat32name
	mov esi,dot
	call display_text
; get cluster index
	mov esi,[ CHK_FD32 ] ; get that dir entry
	xor ebx,ebx
	mov bx,[ esi + fat32dir.clus ] ; starting cluster for file
	mov [ CHK_CLUS ],ebx
	mov [ FIX_CLUS ],ebx
; get total byte size for file
	mov ecx,[ esi + fat32dir.size ] ; file size in bytes
	mov [ ELF_SIZE ],ecx
; get first 4kb of elf file
	xor eax,eax
	mov [ CHK_SECT ],eax
	mov eax,ELFSTAGE
	call load4kpage
	sub dword[ ELF_SIZE ],0x1000
	jc .done
; continue only if header valid
	call elf32chk
	jz short .main
	mov esi,e4r
	jmp say_halt
.main:
	mov eax,ELFSTAGE
	add eax,[ ELF_OFFS ]
	mov [ SEG_HEAD ],eax
.loop:
	mov esi,[ SEG_HEAD ]
; get segment type - skip if not LOAD
	cmp dword[ esi+elf32_seghead.p_type ],1
	jnz .skip
; get alignment - only 4k 'supported'?
	cmp dword[ esi+elf32_seghead.p_algn ],0x1000
	jnz .done
; debug display
	;call display_newline
	;mov eax,[ esi+elf32_seghead.p_type ]
	;call display_value_hexa
	;mov al,' '
	;call display_put
	;mov eax,[ esi+elf32_seghead.p_offs ]
	;call display_value_hexa
	;mov al,' '
	;call display_put
	;mov eax,[ esi+elf32_seghead.p_padd ]
	;call display_value_hexa
	;mov al,' '
	;call display_put
	;mov eax,[ esi+elf32_seghead.p_fsiz ]
	;call display_value_hexa
; load it!
	mov eax,[ esi+elf32_seghead.p_fsiz ]
	mov [ TGT_SIZE ],eax
	mov eax,[ esi+elf32_seghead.p_padd ]
	mov [ TGT_INIT ],eax
	mov ebx,[ esi+elf32_seghead.p_vadd ]
	cmp ebx,eax ; check if physical = virtual
	jz .load
	mov ecx,[ ELF_INIT ]
	sub ecx,ebx ; check if within page
	jc .load
	cmp ecx,0x1000 ; if smaller than page size, entry is here!
	jnc .load
	add ecx,[ TGT_INIT ]
	mov [ PHY_INIT ],ecx
.load:
	mov eax,[ FIX_CLUS ]
	mov [ CHK_CLUS ],eax
	mov eax,[ esi+elf32_seghead.p_offs ]
	call do_offset
.more:
	;call display_newline
	;mov eax,[ TGT_INIT ]
	;call display_value_hexa
	;mov al,' '
	;call display_put
	;mov eax,[ CHK_CLUS ]
	;call display_value_hexa
	mov ecx,[ TGT_SIZE ]
	mov eax,[ TGT_INIT ]
	call load4kpage
	sub dword[ ELF_SIZE ],0x1000
	jnc .okay
	jnz .okay
	mov eax,[ TGT_SIZE ]
	cmp eax,0x1000
	jc .done
.okay:
	add dword[ TGT_INIT ],0x1000
	sub dword[ TGT_SIZE ],0x1000
	jc .skip
	jnz .more
.skip: ; get next
	mov eax,[ SEG_HEAD ]
	add eax,[ ELF_HSIZ ]
	mov [ SEG_HEAD ],eax
	dec byte[ ELF_CODE ]
	jnz .loop
	jmp start
.done:
	mov esi,e3r
	jmp say_halt

; sub-routine to find file offset within fat32 file cluster
; - preserves CHK_SECT/CHK_SIZE for load4kpage to use!
do_offset:
	mov [ CHK_TEMP ],eax ; offset to look for
.next: ; start reading (new) cluster
	xor ecx,ecx
	mov cl,[ INITTEMP + fat_common.spclus ]
	mov [ CHK_SIZE ],cl
	call cl2sect
.read:
	mov edi,ELFSTEMP
	mov edx,[ CHK_SECT ]
	call readsect
	inc dword[ CHK_SECT ]
	dec dword[ CHK_SIZE ]
	sub dword[ CHK_TEMP ],SECT_SIZE
	jz .done
	jc .done
	mov eax,[ CHK_SIZE ]
	or eax,eax
	jnz short .read
	call cl2next
	jmp .next
.done:
	mov eax,[ CHK_SIZE ]
	or eax,eax
	jnz short .back
	xor eax,eax
	mov [ CHK_SECT ],eax
	call cl2next
.back:
	ret

; sub-routine to read a 4kb page from fat32 file cluster
; - preserves CHK_SECT/CHK_SIZE in case stopped within a cluster!
; - will clear CHK_SECT/CHK_SIZE when done...
load4kpage:
	mov [ CUR_SIZE ],ecx
	mov [ ELF_TEMP ],eax
	mov dword[ CHK_TEMP ],0x1000 ; 4kb
; check if continuing from previous
	mov eax,[ CHK_SECT ]
	or eax,eax
	jnz .read
.next: ; start reading (new) cluster
	; get cluster size
	xor ecx,ecx
	mov cl,[ INITTEMP + fat_common.spclus ]
	mov [ CHK_SIZE ],cl
	call cl2sect
.read: ; read single sector
	;call display_newline
	;mov eax,[ ELF_TEMP ]
	;call display_value_hexa
	;mov al,' '
	;call display_put
	;mov eax,[ CHK_CLUS ]
	;call display_value_hexa
	;mov al,' '
	;call display_put
	;mov eax,[ CHK_SECT ]
	;call display_value_hexa
	mov edi,[ ELF_TEMP ]
	mov edx,[ CHK_SECT ]
	call readsect
	inc dword[ CHK_SECT ]
	dec dword[ CHK_SIZE ]
	add dword[ ELF_TEMP ],SECT_SIZE
	sub dword[ CUR_SIZE ],SECT_SIZE
	jz .done
	jc .done
	sub dword[ CHK_TEMP ],SECT_SIZE
	jz .done ; hopefully 4k is multiple of SECT_SIZE
	jc .halt ; not right!
	mov eax,[ CHK_SIZE ]
	or eax,eax
	jnz short .read
; clear for new cluster
	xor eax,eax
	mov [ CHK_SECT ],eax
; get next cluster!
	call cl2next
	jmp .next
.done:
	mov eax,[ CHK_SIZE ]
	or eax,eax
	jnz short .back
	xor eax,eax
	mov [ CHK_SECT ],eax
	call cl2next
	jmp short .back
.halt:
	pop eax
	mov esi,e5r
	jmp say_halt
.back:
	ret

; sub-routine to check elf header
elf32chk:
	mov eax,[ ELFSTAGE ]
	cmp eax,[ ELFMAGIC ]
	jnz .back
	mov eax,[ ELFSTAGE+4 ]
	cmp eax,[ ELFCHECK ]
	jnz .back
	; info
	mov eax,[ ELFSTAGE+elf32_header.elf_entry ]
	mov [ ELF_INIT ],eax
	mov [ PHY_INIT ],eax
	xor eax,eax
	mov ax,[ ELFSTAGE+elf32_header.elf_hsize ]
	mov [ ELF_OFFS ],eax ; prepare reading header
	mov ax,[ ELFSTAGE+elf32_header.elf_phnum ]
	mov [ ELF_CODE ],eax
	mov ax,[ ELFSTAGE+elf32_header.elf_phsiz ]
	mov [ ELF_HSIZ ],eax
	xor eax,eax ; set zero flag!
.back:
	ret

; nay and halt
stop:
	call say_nay
	jmp halt

; done loading - execute!
start:
	mov eax,[ PHY_INIT ]
	jmp eax

; give up - halt!
	mov esi,e6r
; error exit point
say_halt:
	call display_text
halt:
	hlt
	jmp halt

; sub-routine to show we are happy
say_yay:
	mov esi,yay
	call display_text
	ret

; sub-routine to show we are sad
say_nay:
	mov esi,nay
	call display_text
	ret
