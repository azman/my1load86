bits 32

; multiboot header
MBOOT_PAGE_ALIGN    equ 1<<0
MBOOT_MEM_INFO      equ 1<<1
MBOOT_HEADER_MAGIC  equ 0x1BADB002
MBOOT_HEADER_FLAGS  equ MBOOT_PAGE_ALIGN | MBOOT_MEM_INFO
MBOOT_CHECKSUM      equ -(MBOOT_HEADER_MAGIC + MBOOT_HEADER_FLAGS)

section .mboot
align 4
global mboot
mboot:
	dd MBOOT_HEADER_MAGIC
	dd MBOOT_HEADER_FLAGS
	dd MBOOT_CHECKSUM
	dd bdata
	dd bstack

section .bootstrap_data
align 4
global bdata
bdata:
times 16384 db 0

section .bootstrap_stack
align 4
global stack
times 16384 db 0
bstack:

section .text
global start
start:
	cli
	; when from multiboot...
	; - eax should contain 0x2BADB002?
	; - ebx ???
	lgdt [dummygdt]
	jmp 0x08:upper
upper:
	mov eax, 0x10
	mov ds, eax
	mov es, eax
	mov ss, eax
	mov fs, eax
	mov gs, eax
	mov esp, bstack
	push mboot
	extern main
	call main
.halt:
	hlt
	jmp .halt

section .setup
gdt_beg:
	dd 0,0
	db 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x9A, 0xCF, 0x00
	db 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x92, 0xCF, 0x00
gdt_end:
dummygdt:
	dw gdt_end - gdt_beg - 1
	dd gdt_beg
	dw 0 ; nicer if aligned? :)
