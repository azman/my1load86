bits 16
;org 0x7c00
org 0x0500 ; relocating to this address

_boot equ 0x7c00
_bsig equ (_boot+510)
_init equ 0x0500
stack equ 0x7c00
drvno equ (stack-2)

; begin with initializing segments and stack
begin:
	xor ax,ax
	;mov cs,ax ; not needed - will change after jump
	mov ds,ax
	mov ss,ax
	mov es,ax
; prepare stack
	mov sp,stack
	push dx ; save drive index (dl) in driveno!
; move this code elsewhere
	mov si,_boot
	mov di,_init
	mov cx,512/2 ; using word transfer (half sector size)
	rep
	movsw  ; ds:si to es:di, both si & di auto-inc!
; clear boot signature - just to make sure :p
	mov word [_bsig],0x0000
; go to new location @ 0x0000:0x0500+offset
	jmp 0x0000:scan

; structure for partition table - definition only!
struc ptable
	.bflag: resb 1 ; active partition flag (0x80: active, 0x00: inactive)
	.ihead: resb 1 ; (head)
	.isect: resb 1 ; (sect & 0x3f)
	.ilcyl: resb 1 ; (lcyl & 0xff | ((sect & 0xc0) << 2))
	.sysid: resb 1 ; (0x0c = W95 FAT32 LBA, 0x83 = Linux)
	.ehead: resb 1 ; (head)
	.esect: resb 1 ; (sect & 0x3f)
	.elcyl: resb 1 ; (lcyl & 0xff | ((sect & 0xc0) << 2))
	.rsect: resd 1 ; lba first sector
	.tsect: resd 1 ; total sector
endstruc

; scan for bootable partition
scan:
	;xor ax,ax ; should still be zero
	mov bx,4
	mov si,ptab
.loop:
	mov al,[si]
	cmp al,0x80 ; should we just check MSB? => and al,0x80 ?
	jnz short .next
	inc ah
	mov bp,si
.next:
	add si,ptable_size
	dec bx
	jnz short .loop
.done:
	cmp ah,0x01
	jc short .fail
	jz short prep
	call error_exit
	db 'ERROR:>1 boot!',0
.fail:
	call error_exit
	db 'ERROR:No boot!',0

; structure for disk address packet - definition only!
struc dapack
	.size: resb 1 ; size @ 16
	.zero: resb 1 ; always 0
	.secc: resw 1 ; sector count
	.boff: resw 1 ; buffer offset
	.bseg: resw 1 ; buffer segment
	.lbal: resw 1 ; lba lo
	.lbah: resw 1 ; lba hi
	.lbau: resd 1 ; lba up => 48-bit lba?
endstruc

; prepare partition info - always read lba?
prep:
	mov si,bp
	mov ax,[si + ptable.rsect + 2]
	mov [tdapack + dapack.lbah],ax
	mov ax,[si + ptable.rsect]
	mov [tdapack + dapack.lbal],ax
	;jmp info ; skip using int 0x13 extension... for testing load_chs!
; check int 0x13 extension
extm:
	;mov dl,[drvno] ; still there!
	xor cx,cx ; clear for results
	mov bx,0x55aa
	mov ah,0x41 ; function: check extensions
	stc ; make sure really cleared by int 0x13!
	int 0x13 ; disk interrupt
	jc short info
	cmp bx,0xaa55
	jnz short info
	shr cx,1
	jnc short info
	jmp load_lba
; get drive info? to convert lba to chs
info:
	mov ah,8 ; function: read drive param
	int 0x13 ; disk interrupt
	jnc short .next
	call error_exit
	db 'ERROR:I13F08',0
.next:
	and cx,0x3f ; this is SPT (sectors per track) @cl
	xor bx,bx
	inc dh
	mov bl,dh ; this is HPC (heads per cylinder) @bl
; get chs from lba
calc:
	mov ax,[tdapack + dapack.lbal]
	mov dx,[tdapack + dapack.lbah]
	; temp1 = lba / SPT, temp2 = lba % SPT
	div cx ; quotient in ax (temp1), remainder in dx (temp2)
	; s = temp2 + 1
	inc dx
	and dx,0x3f
	mov cx,dx
	xor dx,dx
	; c = temp1 / HPC
	; h = temp1 % HPC
	div bx ; quotient in ax (c), remainder in dx (h)
	mov ch,al
	shl ah,6
	or cl,ah
	mov dh,dl
	mov dl,[drvno]
	mov bx,_boot
	jmp load_chs

; fill in - 218 bytes executable code
	times 218-($-$$) db 0
; 2 bytes (always 0x00?)
	db 0x00,0x00
; 4 bytes disk timestamp (format?)
	db 0x00,0x00,0x00,0x00

; more area for executable code (216 bytes)

; load vbr using good old chs
load_chs:
	mov ax,0x0201 ; function: read sector CHS mode, load single sector
	int 0x13 ; disk interrupt
	jc short .error
	jmp short boot
.error:
	call error_exit
	db 'ERROR:I13F02',0

; load vbr using extended mode
load_lba:
	mov si,tdapack
	mov dl,[drvno]
	mov ah,0x42
	int 0x13
	jc short .error
	jmp short boot
.error:
	call error_exit
	db 'ERROR:I13F42',0

; boot, baby... boot!
boot:
	mov dl,[drvno] ; preserve this!?
	mov ax,[_bsig]
	cmp ax,0xaa55
	jz _boot ; boot if signature found!
	call error_exit
	db 'ERROR:No bsig!',0

; sub-routine for display
bios_print:
	;pusha
.loop:
	lodsb ; ds:si to acc, si auto-inc!
	or al, al
	jz short .done
	mov ah, 0x0e ; function: display char (tty?)
	int 0x10 ; video display interrupt
	jmp short .loop
.done:
	;popa
	ret

; nice way of handling error - got this from syslinux!
error_exit:
	pop si
	call bios_print
	;int 0x18 ; boot failure? execute rom bios?
.halt:
	hlt
	jmp .halt

; disk address packet buffer
tdapack:
	db 0x10
	db 0x00
	dw 0x0001 ; load single sector
	dw _boot
	dw 0x0000
	dw 0x0000
	dw 0x0000
	dd 0x00000000

; fill in - 216 bytes excutable code
	times 440-($-$$) db 0
; 4 bytes disk signature
	db 'MY1',0x00
; 2 bytes (always 0x00?)
	db 0x00,0x00

; partition (4 primary) entries 
ptab:
	times 510-($-$$) db 0

; the boot signature 0xAA55
	db 0x55,0xAA

; 20140916 - size: 342 bytes (comment out 'times' commands and build)
;          - minus rsvd(2), disk tstamp(4), disk sig(4), rsvd(2), boot sig(2)
;          - actual code size is 342-(2+4+4+2+2)=328
;          - max possible code size is (218+216)=434
;          - utilization is (328/343)*100=75.58%

; emulating modern mbr?
; => 218 bytes executable code
; =>   2 bytes (always 0x00?)
; =>   4 bytes disk timestamp
; => 216 bytes executable code
; =>   4 bytes disk signature
; =>   2 bytes (always 0x00?)
; =>  64 bytes partition entries (4 primary partitionss)
; =>   2 bytes boot signature (0x55 0xAA)

; note: at least 1kB of RAM is available at 0x7c00 on IBM XT- & AT- class!
