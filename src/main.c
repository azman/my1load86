/*----------------------------------------------------------------------------*/
/** provided by asm source */
extern void display_init(void);
extern void display_clear(void);
extern void display_write(char*);
/*----------------------------------------------------------------------------*/
#define KERNEL_WELCOME "\nRunning MY1PLAYOS\n\n"
/*----------------------------------------------------------------------------*/
unsigned char VGA_XPOS, VGA_YPOS;
unsigned short int VGA_ATTR,VGA_BLNK,VGA_TROW,VGA_TCOL;
/*----------------------------------------------------------------------------*/
void main(void *mboot_ptr)
{
	(void) mboot_ptr;
	/**  display */
	display_init();
	display_clear();
	display_write(KERNEL_WELCOME);
	/** should not return! */
	while(1);
}
/*----------------------------------------------------------------------------*/
