bits 16
org 0x7c00

; objective: load second stage loader LDPLAYOS.SYS
;   - load at 0x8000 (0x0000:0x8000)
;   - 0x00007E00-0x0007FFFF guaranteed free for use
;   - within 640KB (confirmed existence?)
; for now, target fat32 partitions...

; three things commonly true for bootloaders
; -  dl = boot drive unit
; -  cs:ip = 0x0000:0x7c00 (not if from BIOS? some go for 0x7c00:0x0000)
; -  ds:si = points to 16-bit MBR partition table entry (NEED THIS!)

_boot equ 0x7c00
stack equ 0x7c00
segld equ 0x0000
offld equ 0x8000
drvno equ (stack-2)
btlba equ (stack-6)
fatst equ (stack-10)
datst equ (stack-14)
clust equ (stack-18)
tempd equ (stack-20)

; fat32 offset jmps by 0x58 (0xeb 0x58 0x90) 0x90 is NOP
	jmp short first
	nop
fat:
	times 0x5a-($-$$) db 0
; some bios use 0x07c0:0x0000 rather than 0x0000:0x7c00
first:
; maintain this in case loaded by bios
	jmp 0x0000:begin ; this will reset cs!
begin:
; initialize stack
	mov sp,stack
	push dx ; save drive index (dl) in driveno!
; ds:si should still hold 16-byte mbr partition table entry => get lba!
	mov eax,[ si + ptable.rsect ] ; 32-bit reg access in real mode!
	push eax ; save lba!
; get reserved sector count
	xor ecx,ecx
	mov cx,[ fat + fat_common.rsccnt ]
	add eax,ecx
	push eax ; save fat starting lba add in fatst
; get fat count - clear upper extended register?
	xor eax,eax
	mov al,[ fat + fat_common.fatcnt ]
; check if we're on fat16 or fat32 - clear upper extended register?
	mov dx,[ fat + fat_common.rooten ] ; 0=fat32, else=fat16/12
	or dx,dx
	jz short ffind
; not supporting other than fat32
	call error_exit
	db '[X]FAT32',0
ffind:
; get fat size (in sector count)
	;xor ecx,ecx ; done earlier when getting rsccnt?
	mov cx,[ fat + fat_fat32.scpfat ]
	mul ecx ; # of fat x sector per fat
	add eax,[ fatst ] ; add reserved sectors and fat offset?
	push eax ; save starting lba address for data cluster in datst
; get first cluster for root directory
	mov eax,[ fat + fat_fat32.clroot ]
	push eax ; save cluster index in clust
; push dummy value for sector per cluster count in tempd
	push cx
.init:
	mov dl,[ fat + fat_common.spclus ]
	mov [ tempd ],dl
	call cl2lba
.read:
	call load_lba
; browse root dir info (1 sector)
	mov dx,buff ; sector is in buff
.loop:
	mov si,dx
	mov al,byte[ si ]
	cmp al,0xe5 ; check if not used
	jz short .skip
	cmp al,0x00 ; done if at end of directory
	jz short .done
; check if subdir? - check attrib?
; - b0: read only, b1: hidden, b2: system, b3: vol label
; - b4: sub dir, b5: archive, b6/7: unused
; skip if vol label, . and .. does not exists for root
	mov al,byte[ si + 11 ]
	and al,0x08
	jnz .skip
; check if this is target file
	mov di,tgt ; si already set!
	mov cx,11
	repe
	cmpsb
	jcxz fetch
; move for printing FOR NOW...
;	mov di,tgt
;	mov cx,11
;	rep
;	movsb  ; ds:si to es:di, both si & di auto-inc!
;	mov si,tgt
;	call bios_print
;	call bios_print_line
.skip:
	add dx,32
	cmp dx,buff+512
	jc short .loop ; false if dx==buff+512!
; done with a sector, check end of cluster
	dec byte[ tempd ]
	jz short .next
	inc dword[ tdapack + dapack.lbal ]
	jmp short .read
.next:
	call clnext
	jnz short .init
.done:
	call error_exit
	db '[X]BLIND',0

fetch:
; get cluster index
	mov si,dx ; get that dir entry
	xor ebx,ebx
	mov bx,[ si + fat32dir.clus ] ; starting cluster
	mov [ clust ], ebx
; get bytes per sector - reducing total size
	;xor ebx,ebx ; upper 16-bit should still be 0???
	mov bx,[ fat + fat_common.bpsect ] ; sector size in bytes
; get total byte size for file
	mov ecx,[ si + fat32dir.size ] ; file size in bytes
; prepare for transfer
	;mov ax,segld
	;mov es,ax
	;xor di,di
	mov di,offld
.init:
	mov dl,[ fat + fat_common.spclus ]
	mov [ tempd ],dl
	call cl2lba
.read:
	call load_lba
; transfer to target
	push cx
	mov si,buff
	mov cx,512/2 ; using word transfer (half sector size)
	rep
	movsw  ; ds:si to es:di, both si & di auto-inc!
	pop cx
; debug
	;mov al,'+'
	;mov ah,0x0e
	;int 0x10
; check if done
	sub ecx,ebx
	jz exec
	jc exec
; done with a sector, check end of cluster
	dec byte[ tempd ]
	jz short .next ; get next cluster
	inc dword[ tdapack + dapack.lbal ]
	jmp short .read ; get next sector
.next:
	call clnext
	jnz short .init
	call error_exit
	db '[E]LOST',0

; done loading - execute!
exec:
	;call error_exit
	;db '#',0
	jmp segld:offld

; sub-routine to load sector to buff! (set in tdapack)
load_lba:
	mov si,tdapack
	mov dl,[drvno]
	mov ah,0x42
	int 0x13
	jc short .error
	ret
.error:
	call error_exit
	db '[E]I13F42',0

; sub-routine to convert cluster index to lba addressing
cl2lba:
	mov eax,[ clust ]
	sub eax,2 ; cluster 0 & 1 is unused - cluster 2 at beginning of data
; get cluster size (in sector count) => cx!
	xor edx,edx
	mov dl,[ fat + fat_common.spclus ]
	mul edx
	add eax,dword[ datst ]
; prepare tdapack here!
	mov [ tdapack + dapack.lbal ],eax
	ret

; sub-routine to find next cluster chain
clnext:
	mov eax,[ clust ]
	shl eax,2 ; times 4 - get fat offset 32-bit per cluster (28 actually)
	; get sector number containing the cluster number
	xor edx,edx
	xor ecx,ecx
	mov cx,[ fat + fat_common.bpsect ] ; sector size in bytes
	div ecx  ; sector offset in ax, byte offset within sector in dx
	add eax,[ fatst ]
	; get cluster number from fat on disk
	mov bx,dx ; should NOT be more than bytes per sector (<4KB?)
	mov ecx,[ tdapack + dapack.lbal ]
	mov [ tdapack + dapack.lbal ],eax
	call load_lba
	mov [ tdapack + dapack.lbal ],ecx
	; get cluster number (byte offset)
	mov si,bx
	mov eax,[ si+buff ]
	cmp eax,0xFFFFFFFF ; actually >0xFFFFFFF8 ?
	jz short .back
	and eax,0x0FFFFFFF ; 28-bits? cluster never be 0 (or 1!)
	mov [ clust ],eax
.back:
	ret

; nice way of handling error - got this from syslinux!
error_exit:
	pop si
.loop:
	lodsb ; ds:si to acc, si auto-inc!
	or al,al
	jnz short .show
.halt:
	hlt
	jmp short .halt
.show:
	mov ah,0x0e ; function: display char
	int 0x10 ; video display interrupt
	jmp short .loop

; target loader file to look for
tgt db 'LDPLAYOSSYS',0

; disk address packet buffer
tdapack:
	db 0x10
	db 0x00
	dw 0x0001 ; load single sector
	dw buff
	dw 0x0000
	dw 0x0000
	dw 0x0000
	dd 0x00000000

; filler
	times 510-($-$$) db 0

; the boot signature 0xAA55
	db 0x55,0xAA

; buffer for cluster?
buff:

; structure for FAT common - definition only!
struc fat_common ; BIOS parameter block
	.oem_id: resb 8 ; 03 - oem id string
	.bpsect: resw 1 ; 11 - bytes per sector : 512,1024,2048,4096
	.spclus: resb 1 ; 13 - sector per cluster : 1,2,4,8,16,32,64,128
	.rsccnt: resw 1 ; 14 - # of reserved sectors (FAT12/16=>1, FAT32=>32)
	.fatcnt: resb 1 ; 16 - # of fat (always 2?)
	.rooten: resw 1 ; 17 - root entries (fat16=>512,fat32=>0)
	.sctsma: resw 1 ; 19 - sectors (fat32=>0, also 0 if smaller than 32M?)
	.medtyp: resb 1 ; 21 - media type (f0-floppy, f8-hard disk, fa-ramdisk?)
	.scpfat: resw 1 ; 22 - sectors per fat (on small vols, fat32=>0)
	.scptrk: resw 1 ; 24 - sectors per track (63?)
	.heads_: resw 1 ; 26 - heads
	.hidsec: resw 1 ; 28 - hidden sectors
endstruc ; size = 27 bytes
; NOTE: cluster size should be <= 32K (rare cases of 64K ok)

; structure for FAT16 - definition only!
struc fat_fat16
	.biospb: resb fat_common_size
	.hidsec: resw 1  ; 30 - hidden sectors (extension?)
	.sctlrg: resd 1  ; 32 - sectors (large volumes)
	.drvnum: resb 1  ; 36 - logical drive num - for use with int13
	.rsvd_1: resb 1  ; 37 - reserved
	.extsig: resb 1  ; 38 - (0x29) ext sig indicate presence (next 3 fields)
	.sernum: resd 1  ; 39 - serial number for partition
	.vollbl: resb 11 ; 43 - volume label or "NO NAME    "
	.fstype: resb 8  ; 54 - fs type FAT12/FAT16/FAT or all 0
endstruc ; size = 59 bytes

; structure for FAT32 - definition only!
struc fat_fat32
	.biospb: resb fat_common_size
	.hidsec: resw 1  ; 30 - hidden sectors (extension?)
	.sctlrg: resd 1  ; 32 - sectors (large volumes)
	.scpfat: resd 1  ; 36 - sectors per fat
	.m_flag: resw 1  ; 40 - mirror flags (b15-b8:rsvd) (b6-b4:rsvd)
					 ; (b7:1=>single active fat,0=>all fats updated @runtime)
					 ; (b3-b0: # of active fat if b7=1)
	.fsvers: resw 1  ; 42 - fs version
	.clroot: resd 1  ; 44 - first cluster of root directory (usu. 2)
	.fsinfo: resw 1  ; 48 - fs info sector num in fat rsvd area (usu. 1)
	.backbs: resw 1  ; 50 - backup boot sector (0xFFFF if none, usu. 6)
	.rsvd_1: resb 12 ; 52 - reserved
	.drvnum: resb 1  ; 64 - logical drive num - for use with int13
	.rsvd_2: resb 1  ; 65 - reserved
	.extsig: resb 1  ; 66 - (0x29) ext sig indicate presence (next 3 fields)
	.sernum: resd 1  ; 67 - serial number for partition
	.vollbl: resb 11 ; 71 - volume label or "NO NAME    "
	.fstype: resb 8  ; 82 - fs type "FAT32   "
endstruc ; size = 87 bytes

; structure for partition table - definition only!
struc ptable
	.bflag: resb 1 ; active partition flag (0x80: active, 0x00: inactive)
	.ihead: resb 1 ; (head)
	.isect: resb 1 ; (sect & 0x3f)
	.ilcyl: resb 1 ; (lcyl & 0xff | ((sect & 0xc0) << 2))
	.sysid: resb 1 ; (0x0c = W95 FAT32 LBA, 0x83 = Linux)
	.ehead: resb 1 ; (head)
	.esect: resb 1 ; (sect & 0x3f)
	.elcyl: resb 1 ; (lcyl & 0xff | ((sect & 0xc0) << 2))
	.rsect: resd 1 ; lba first sector
	.tsect: resd 1 ; total sector
endstruc

; structure for disk address packet - definition only!
struc dapack
	.size: resb 1 ; size @ 16
	.zero: resb 1 ; always 0
	.secc: resw 1 ; sector count
	.boff: resw 1 ; buffer offset
	.bseg: resw 1 ; buffer segment
	.lbal: resw 1 ; lba lo
	.lbah: resw 1 ; lba hi
	.lbau: resd 1 ; lba up => 48-bit lba?
endstruc

; structure for fat32 dir entry
struc fat32dir
	.name: resb 11 ; 8+3, whitespace padded for 8
	.attr: resb 1 ; 6-bits actually?
	.non1: resb 10 ; reserved?
	.time: resw 1 ; 5/6/5 => hh/mm/ds
	.date: resw 1 ; 7/4/5 => yy-1980/mm/dd
	.clus: resw 1 ; first cluster
	.size: resd 1 ; size
endstruc

; 20151028 - size: 420 bytes (comment out 'times' commands and build)
;          - minus jmp-nop(3), boot sig(2)
;          - actual code size is 420-(3+2)=415
;          - max possible code size is (510-90)=420
;          - utilization is (415/420)*100=98.80%
