#!/bin/bash

MY1USBDRV="$1"
[ "$MY1USBDRV" == "" ] && MY1USBDRV="disk.img"

QEMU_BIN="qemu-system-i386"
QEMU_SYS=$(which "$QEMU_BIN" 2>/dev/null)
[ ! -x "$QEMU_SYS" ] &&
	echo "Cannot find QEMU binary '$QEMU_BIN'! Aborting!" && exit 1

[ ! -f "$MY1USBDRV" ] &&
	echo "Cannot find disk image '$MY1USBDRV'! Aborting!" && exit 1

${QEMU_SYS} ${MY1USBDRV}
#${QEMU_BIN} -hda ${MY1USBDRV} -boot c -d cpu_reset
