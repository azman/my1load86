#!/bin/bash

MY1USBDRV=""
MY1IMGLEN=""
PARTLABEL=""
DO_FAT_32=""

QEMU_BIN="qemu-img"
QEMU_IMG=$(which "$QEMU_BIN" 2>/dev/null)
[ ! -x "$QEMU_IMG" ] &&
	echo "Cannot find QEMU binary '$QEMU_BIN'! Aborting!" && exit 1

# parse command parameters
while [ "$1" != "" ]; do
	case $1 in
		--image|-i)
			shift
			MY1USBDRV=$1
			;;
		--size|-s)
			shift
			MY1IMGLEN=$1
			;;
		--fat32)
			DO_FAT_32="YES"
			;;
		--label)
			shift
			PARTLABEL=$1
			;;
		-*)
			echo "Unknown option '$1'"
			exit 1
			;;
		*)
			echo "Unknown parameter '$1'!"
			exit 1
			;;
	esac
	shift
done

# use default settings if necessary
[ "$MY1USBDRV" == "" ] && MY1USBDRV="disk.img"
[ "$MY1IMGLEN" == "" ] && MY1IMGLEN="512M"
[ "$PARTLABEL" == "" ] && PARTLABEL="MY1TESTPART"

# create image file
echo -n "Creating disk image '${MY1USBDRV}' at size '${MY1IMGLEN}'... "
${QEMU_IMG} create -f raw ${MY1USBDRV} ${MY1IMGLEN} >/dev/null
[ $? -ne 0 ] && echo "Failed! Aborting!" && exit 1
echo "done!"

# CHS params provided are bogus values... for now?
echo -n "Creating single partition disk and flag as bootable... "
fdisk -C32 -S63 -H255 ${MY1USBDRV} >/dev/null 2>&1 <<EOF
n
p
1


t
c
a
1
w
EOF
[ $? -ne 0 ] && echo "Failed! Aborting!" && exit 1
echo "done!"

# create fat32 filesystem... if requested
if [ "$DO_FAT_32" == "YES" ] ; then
	echo -n "Formatting FAT32 partition on '${MY1USBDRV}'... "
	OPTS="conv=notrunc seek=1048576"
	MY1TEMP="disk_temp.img"
	PART_SIZE=$(($(fdisk -s ${MY1USBDRV})-1024))
	dd if=/dev/zero of=${MY1TEMP} bs=1024 count=${PART_SIZE} 2>/dev/null
	mkdosfs -F 32 -n "$PARTLABEL" ${MY1TEMP} >/dev/null
	dd if=${MY1TEMP} of=${MY1USBDRV} bs=1 count=${PART_SIZE} ${OPTS} 2>/dev/null
	rm ${MY1TEMP}
	[ $? -ne 0 ] && echo "Failed! Aborting!" && exit 1
	echo "done!"
fi
